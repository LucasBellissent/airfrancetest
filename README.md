# AirFranceTest

Request
There are tree rootes :
Get user by id :
At : Http://localhost:8081/user/{id}
Here {id} is the id of the user you are looking for.
This root will return every informations of the user you asked for.
If it doesn’t exist you will receive a faillure message.
Register user :
At : Http://localhost:8081/user/save?{body}
This root permit to register a user.
There is some mandatory fields that you need to send to this adress:
- « email » type String
- « age » (only user older than 17 are allowed to be register) type Long
- « country » (only person who live in France are allowed to be register) type String
And optional field:
- « drivingLicence » (default : false) type Boolean

## Getting started

From here, I’ll assume you have the last version of this project, JDK 8.0, IntelliJ, Maven
and Postman installed.

Db :

go to : https://www.mongodb.com/try/download/community
Select MongoDb locally.
MongoDB Community Server.
5.0.6 version, then download and Install It.
On MongoDB Compass, click on « connect »
Create a « new Database ».
Name Database as you wish and keep the name, but the collection name needs to be « user ».
You have sucessfully install your database.

Server :

Open the projet with intellji
go to : src/main/resources/application.properties
Configure the port of the server :
Server.port=
On Mongodb Compass, check host and the name of your database.
Complete spirng.data.mongodb with your db configuration.

Install Maven dependances :
Mvn install

Start the project :
On intellij go to src/main/java/com/airfrance/test/
Click left on « TestApplication » and « run ‘TestApplication’».
If there is no error message, this means that you sucessfully install and start the application.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/LucasBellissent/airfrancetest.git
git branch -M main
git push -uf origin main
```

## Test

Postman :

To test it we need to send request to our server.
For this, go to home page to « create new », then you select « http request ».
Now you have two options, you can try the GET or the POST request.
Let’s start with the registration.

Register user :
Click on the GET and select POST
Click on « body », then « row »
On the url copy/past this : Http://localhost:8081/user/save
On the body : { "email" : "tests@gmai.com", "country" : "Fr", "age" : "18", "drivingLicence" : true }
Then click on send.
If there is writen «User added successfully »
That means that registration works.

Get user by id:
go to MongoDB Compass -> the name of your database -> user
find the user that you just register, and copy the id
On the url copy/past this : Http://localhost:8081/user/{id}
Remplace {id} by the id you copy.
Then send the request.
You should have the same result as you saw in your database.

Get user by email:
same as user by id
but now the root is : http://localhost:8081/user/email
you have to click on « Params » and add « key » : emaail ; « Value » : {the email you are looking for}
then you can send the request
Unit Tests
You can also test the app going on intellij to src/test/java/com/airfrance/test/ Click left on « TestApplicationTests » and « run ‘TestApplicationTests’».

***