package com.airfrance.test.service;

import com.airfrance.test.repository.UserRepository;
import com.airfrance.test.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    private UserService userService;

    @Test
    void shouldFindByEmail() throws Exception {
        userService = new UserService(userRepository);
        User ok = User.builder().unid("1").email("michael@gmail.com").country("france").age(18L).drivingLicence(false).build();

        String emailOk = "michael@gmail.com";
        String emailWrong = "mich@gmail.com";
        String emailWrong2 = null;

        when(userRepository.findByEmail(emailOk)).thenReturn(ok);

        User verify = userService.findByEmail(emailOk);
        assertEquals(ok, verify);

        Exception  thrown = Assertions.assertThrows(Exception.class, () -> {
            userService.findByEmail(emailWrong);
        });
        assertEquals(thrown.getMessage(), "no match for this user email");

        thrown = Assertions.assertThrows(Exception.class, () -> {
            userService.findByEmail(emailWrong2);
        });
        assertEquals(thrown.getMessage(), "no match for this user email");
    }

    @Test
    void shouldCreate() throws Exception {
        userService = new UserService(userRepository);
        //test champs null
        //test chaque champ avec une erreur verifier le bon message de retour
       //supposed to be ok
        User user1 =  User.builder().unid("1").email("michael@gmail.com").country("france").age(18L).build();
        //email incorrect
        User user2 =  User.builder().unid("1").email("michael").country("france").age(18L).build();
        //too young
        User user3 =  User.builder().unid("1").email("jeanlouis@gmail.com").country("france").age(17L).build();
        //not the good country
        User user4 =  User.builder().unid("1").email("jeanlouis@gmail.com").country("germany").age(18L).build();
        //One other good
        User user5 =  User.builder().unid("2").email("jeanlouis@gmail.com").country("fR").age(25L).build();

        User ok = User.builder().unid("1").email("michael@gmail.com").country("france").age(18L).drivingLicence(false).build();
        User ok2 = User.builder().unid("2").email("jeanlouis@gmail.com").country("fR").age(25L).drivingLicence(false).build();

        // test null
        User null1 =  User.builder().unid("2").email(null).country("fR").age(25L).build();
        User null2 =  User.builder().unid("2").email("jeanlouis@gmail.com").country(null).age(25L).build();
        User null3 =  User.builder().unid("2").email("jeanlouis@gmail.com").country("fR").age(null).build();
        User null4 =  User.builder().unid("2").email("jeanlouis@gmail.com").country("fR").age(25L).drivingLicence(null).build();


        //ok
        when(userRepository.save(ok)).thenReturn(ok);
        User verify = userService.create(user1);
        assertEquals(ok, verify);

        //email error
        Exception  thrown = Assertions.assertThrows(Exception.class, () -> {
            userService.create(user2);
        });
        assertEquals(thrown.getMessage(), "The user email is not valid");

        //null email
        thrown = Assertions.assertThrows(Exception.class, () -> {
            userService.create(null1);
        });
        assertEquals(thrown.getMessage(), "The user email is not valid");


        //email allready exist
        when(userRepository.findByEmail("michael@gmail.com")).thenReturn(ok);
        thrown = Assertions.assertThrows(Exception.class, () -> {
            userService.create(user1);
        });
        assertEquals(thrown.getMessage(), "The user email is already taken");

        //age  error
        thrown = Assertions.assertThrows(Exception.class, () -> {
            userService.create(user3);
        });
        assertEquals(thrown.getMessage(), "The user have to be older");

        //null age
        thrown = Assertions.assertThrows(Exception.class, () -> {
            userService.create(null3);
        });
        assertEquals(thrown.getMessage(), "The user have to be older");

        //country error
        thrown = Assertions.assertThrows(Exception.class, () -> {
            userService.create(user4);
        });
        assertEquals(thrown.getMessage(), "The user have to live in France");

        //null country
        thrown = Assertions.assertThrows(Exception.class, () -> {
            userService.create(null2);
        });
        assertEquals(thrown.getMessage(), "The user have to live in France");


        //null DrivingLicence
        when(userRepository.save(ok2)).thenReturn(ok2);
        verify = userService.create(null4);
        assertEquals(ok2, verify);

        verify = userService.create(user5);
        assertEquals(ok2, verify);
    }
}