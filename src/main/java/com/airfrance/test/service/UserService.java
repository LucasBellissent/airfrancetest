package com.airfrance.test.service;

import com.airfrance.test.model.User;
import com.airfrance.test.repository.UserRepository;
import com.nimbusds.oauth2.sdk.util.StringUtils;
import org.apache.commons.validator.EmailValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Locale;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public User findById(String id) {
       return this.userRepository.findById(id).orElseThrow(() ->
       new RuntimeException(new Exception("no match for this user id")));
    }

    public User findByEmail(String email) throws Exception {
        User user = this.userRepository.findByEmail(email);
        if (user == null )
            throw new Exception("no match for this user email");
        else {
            return user;
        }
    }

    public User create(User body) throws Exception {
        String country = body.getCountry();
        String email = body.getEmail();
        EmailValidator validator = EmailValidator.getInstance();


        if (StringUtils.isBlank(email) || !(validator.isValid(email))) {
            throw new Exception("The user email is not valid");
        }

        if (!(userRepository.findByEmail(email) == null))
            throw new Exception("The user email is already taken");


        if (body.getAge() == null || body.getAge() < 18L){
            throw new Exception("The user have to be older");
        }

        if (StringUtils.isBlank(country) ||
                !(country.toLowerCase(Locale.ROOT).equals("france") ||
                        country.toLowerCase(Locale.ROOT).equals("fr"))) {
            throw new Exception("The user have to live in France");
        }

        if (body.getDrivingLicence() == null) {
            body.setDrivingLicence(false);
        }

        return this.userRepository.save(body);
    }
}
