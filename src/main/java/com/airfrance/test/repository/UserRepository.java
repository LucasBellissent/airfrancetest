package com.airfrance.test.repository;

import com.airfrance.test.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    @Query("{email: ?0}")
    User findByEmail(@Param("email") String email);
}
