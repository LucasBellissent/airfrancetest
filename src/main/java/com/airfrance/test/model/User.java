package com.airfrance.test.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * User
 */
@Document(collection = "user")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    private String unid;
    private String email;
    private String country;
    private Long age;
    private Boolean drivingLicence;


    @Override
    public String toString() {
        return "User{" +
                "unid=" + unid +
                ", email='" + email + '\'' +
                ", country='" + country + '\'' +
                ", age=" + age + '\'' +
                ", drivingLicence=" + drivingLicence +
                '}';
    }
}
