package com.airfrance.test.controller;

import com.airfrance.test.model.User;
import com.airfrance.test.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * /user
 * Here you can register or get a user
 */
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    @Autowired
    private final UserService userService;
    /**
     * /save
     * Here you can save a user
     * @param user  email this is the email of the User, this param is not optional
     * @param user age this is the age in number of the User, this param is not optional
     * @param user country this is the country of the User, this param is not optional
     * @param user drivingLicence if the user have a driving Licence, this param should be true, this param is optional
     * @return 200 User with a message  "added successfully"
     * @throws 500 with a justification of why the request failed
     */
    @PostMapping(value = "/save")
    public ResponseEntity<?> createUser(@RequestBody User user) throws Exception {
        this.userService.create(user);
        return ResponseEntity.ok("User added successfully");
    }

    /**
     * /{id}
     * Here search a user by id
     * @param id of the user you are looking for
     * @return 200 with the information of the user
     * @throws 500 with a justification of why the request failed
     */
    @GetMapping(value = "/{id}")
    public User getUser(@PathVariable String id) {
        return this.userService.findById(id);
    }

    /**
     * /email
     * Here search a user by email
     * @param email of the user you are looking for
     * @return 200 with the information of the user
     * @throws 500 with a justification of why the request failed
     */
    @GetMapping(value = "/email")
    public User getUserByEmail(@RequestParam(name="email") String email) throws Exception {
        return this.userService.findByEmail(email);
    }
}
